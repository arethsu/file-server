let path = require('path');
var http = require('http');

let express = require('express');
let logger = require('morgan');

let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');

let app = express();

// Set application settings
app.set('mount', path.join('~', 'Documents', 'server-files'));
app.set('access_codes', ['lukas', 'ben', 'pontus']);

// TODO: Research different API authentication methods
app.use(logger('dev')); // Logger with nice colored terminal output
app.use(cookieParser('secret banana')); // Cookie parser with secret
app.use(bodyParser.json()); // Body parser for JSON
app.use(bodyParser.urlencoded({ extended: true })); // Body parser for URL encoded data
app.use(express.static('public')); // Static content server

// Add security headers
// TODO: Document all headers
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');

    res.header('Content-Security-Policy', "default-src 'self'");
    res.header('Referrer-Policy', 'no-referrer');

    next();
});

// Send error in case of error
// TODO: Add proper logging
app.use((err, req, res, next) => {
    console.error(err.message);
    console.error(err.stack);
    res.sendStatus(500);
});

app.get('/', (req, res) => {
  res.send('hello');
});


// Start server
// TODO: Get parameters from the terminal, such as address and port
const address = process.env.ADDRESS || '0.0.0.0';
const port = process.env.PORT || '3000';
app.listen(port, address, () => console.log('API running on ' + address + ':' + port));
